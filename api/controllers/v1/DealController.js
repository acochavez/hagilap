var TAG = '[DealController]';

module.exports = {

  addDeal: function (req, res) {
    var ACTION = '[create]';
    LogService.log('debug', TAG + ACTION + ' request body', req.body);
    async.auto({
      validation: function (callback) {
        var keys = ['name','description'];
        callback(ValidationService.containKeys(keys, req.body));
      },

      create: ['validation', function (callback) {
        req.body.createdBy = req.session.merchantId;

        Deal.create(req.body, function (err, result) {
          if (err) {
            LogService.log('error', TAG + ACTION, err);
            callback(ErrorService.raise('DB_ERROR'));
          } else {
            callback(null, result);
          };
        });
      }]
    }, function (error, output) {
      var resp = {};

      if (error) {
        resp = { error: error.error };
        LogService.log('error', TAG + ACTION + ' response', resp);
        res.json(error.status, resp);
      } else {
        resp = { success: 'ok', dealId: output.create.id };
        LogService.log('debug', TAG + ACTION + ' response', resp);
        res.json(200, resp);
      }
    });
  },

  getAllDeals: function (req, res) {
    var ACTION = '[getAllDeals]';

    LogService.log('debug', TAG + ACTION + ' request body', req.body);

    async.auto({
      find: function (callback, cbres) {
        var query = {
          createdBy: req.params.merchantId
        };

        Deal.find(query, function (err, result) {
          if (err) {
            LogService.log('error', TAG + ACTION, err);
            callback(ErrorService.raise('DB_ERROR'));
          } else {
            for (var i in result) {
              result[i] = result[i].toJSON();
            };

            var obj  = {
              deals: result,
              total: result.length
            };
            callback(null, obj);
          }
        })
      }

    }, function (error, output) {
      var resp = {};

      if (error) {
        resp = { error: error.error };
        LogService.log('error', TAG + ACTION + ' response', err);
        res.json(err.status, {error: err.error});
      } else {
        LogService.log('debug', TAG + ACTION + ' response', output.find);
        res.json(200, output.find);
      }
    });
  },
}
