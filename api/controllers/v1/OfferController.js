var TAG = '[OfferController]';
var geolib = require('geolib');

module.exports = {
  newOffer: function (req, res) {
    var ACTION = '[newOffer]';
    LogService.log('debug', TAG + ACTION + ' request body', req.body);

    async.auto({
      validation: function (callback) {
        var keys = ['location'];
        callback(ValidationService.containKeys(keys, req.body || {}));
      },

      findUser: ['validation', function (callback, cbres) {
        User.findOne({id: req.query.userid}, function (err, result) {
          if (err) {
            LogService.log('error', TAG + ACTION, err);
            callback(ErrorService.raise('DB_ERROR'));
          } else {
            // Flatten location
            req.body.location = [req.body.location.longitude, req.body.location.latitude];

            callback(null, result);

            // Background
            // result.location = req.body.location;
            // result.save(function (err) {
            //   if (err) LogService.log('error', TAG + ACTION, err);
            // })
          };
        })
      }],

      findInquiry: ['validation', function (callback) {
        Inquiry.findOne({id: req.params.inquiryId, status: 'PENDING'})
          .populate('createdBy')
          .exec(function (err, data) {
          if (err) {
            LogService.log('error', TAG + ACTION, err);
            callback(ErrorService.raise('DB_ERROR'));
          } else if (!data) {
            LogService.log('error', TAG + ACTION, 'No such inquiry');
            callback(ErrorService.raise('NOT_FOUND'));
          } else {
            callback(null, data);
          }
        })
      }],

      create: ['findInquiry', function (callback) {
        req.body.createdBy = req.query.userid;
        req.body.forInquiry = req.params.inquiryId;

        Offer.create(req.body, function (err, result) {
          if (err) {
            LogService.log('error', TAG + ACTION, err);
            callback(ErrorService.raise('DB_ERROR'));
          } else {
            callback(null, result);
          };
        });
      }],

      pushToInquirer: ['create', 'findUser', function (callback, cbres) {
        var inquiryData = cbres.findInquiry;
        var userData = cbres.findUser;
        var offerData = cbres.create;
        var message = {
          type: 'OFFER',
          offerId: offerData.id,
          status: offerData.status,
          details: inquiryData.toJSON(),
          profile: {
            name: userData.name,
            age: userData.age,
            gender: userData.gender,
            userType: userData.userType,
            medPrac: userData.medPrac,
            location: {
              longitude: req.body.location[0],
              latitude: req.body.location[1]
            }
          },
          distance: (geolib.getDistance({latitude: req.body.location[1], longitude: req.body.location[0]},
                                        {latitude: inquiryData.createdBy.location[1], longitude: inquiryData.createdBy.location[0]}) / 1000)  // in KM
        };

        // Add to data
        var dataBody = {
          type: 'OFFER',
          status: offerData.status,
          details: inquiryData.toJSON(),
          profile: userData
        };
        DataService.log(dataBody);

        // Send push notifs
        GCMService.send([inquiryData.createdBy.gcmToken], message, function (err, response) {
          if (err || (response && response.failure > 0)) {
            LogService.log('error', TAG + ACTION, err || response);
            return callback(ErrorService.raise('PUSH_FAILED'));
          } else {
            return callback(null);
          }
        });
      }]
    }, function (error, output) {
      var resp = {};

      if (error) {
        resp = { error: error.error };
        LogService.log('error', TAG + ACTION + ' response', resp);
        res.json(error.status, resp);
      } else {
        resp = { success: 'ok', offerId: output.create.id };
        LogService.log('debug', TAG + ACTION + ' response', resp);
        res.json(200, resp);
      }
    });
  },

  getOffer: function (req, res) {
    var ACTION = '[getOffer]';
    LogService.log('debug', TAG + ACTION + ' request body', req.body);

    async.auto({
      findUser: function (callback, cbres) {
        User.findOne({id: req.query.userid}, function (err, result) {
          if (err) {
            LogService.log('error', TAG + ACTION, err);
            callback(ErrorService.raise('DB_ERROR'));
          } else {
            callback(null, result);
          };
        })
      },

      findInquiry: function (callback) {
        Inquiry.findOne({id: req.params.inquiryId, status: 'PENDING'})
          .populate('createdBy')
          .exec(function (err, data) {
          if (err) {
            LogService.log('error', TAG + ACTION, err);
            callback(ErrorService.raise('DB_ERROR'));
          } else if (!data) {
            LogService.log('error', TAG + ACTION, 'No such inquiry');
            callback(ErrorService.raise('NOT_FOUND'));
          } else {
            // Hold this inquiry
            data.status = 'HOLD';

            callback(null, data);

            data.save(function (err) {
              if (err) {
                LogService.log('error', TAG + ACTION, err);
              }
            })
          }
        })
      },

      findOffer: function (callback) {
        Offer.findOne({id: req.params.offerId, status: 'AWAITING'})
          .populate('createdBy')
          .exec(function (err, data) {
          if (err) {
            LogService.log('error', TAG + ACTION, err);
            callback(ErrorService.raise('DB_ERROR'));
          } else if (!data) {
            LogService.log('error', TAG + ACTION, 'No such offer');
            callback(ErrorService.raise('NOT_FOUND'));
          } else {
            callback(null, data);
          }
        })
      },

      findOthers: function (callback) {
        Offer.find({forInquiry: req.params.inquiryId, id: { '!': req.params.offerId }, status: 'AWAITING'})
          .populate('createdBy')
          .exec(function (err, data) {
          if (err) {
            LogService.log('error', TAG + ACTION, err);
            callback(ErrorService.raise('DB_ERROR'));
          } else {
            callback(null, data);
          }
        })
      },

      accept: ['findUser', 'findInquiry', 'findOffer', function (callback, cbres) {
        var inquiryData = cbres.findInquiry;
        var offerData = cbres.findOffer;
        var userData = cbres.findUser;

        // Update offer status
        offerData.status = 'ACCEPTED';

        // Flatten location

        var message = {
          type: 'OFFER',
          offerId: offerData.id,
          status: offerData.status,
          details: inquiryData.toJSON(),
          profile: {
            name: userData.name,
            age: userData.age,
            gender: userData.gender,
            userType: userData.userType,
            medPrac: userData.medPrac,
            location: {
              longitude: userData.location[0],
              latitude: userData.location[1]
            }
          },
          distance: (geolib.getDistance({latitude: offerData.createdBy.location[1], longitude: offerData.createdBy.location[0]},
                                        {latitude: inquiryData.createdBy.location[1], longitude: inquiryData.createdBy.location[0]}) / 1000)  // in KM
        };

        // Add to data
        var dataBody = {
          type: 'OFFER',
          status: offerData.status,
          details: inquiryData.toJSON(),
          profile: offerData.createdBy
        };
        DataService.log(dataBody);

        // Send push notifs
        GCMService.send([offerData.createdBy.gcmToken], message, function (err, response) {
          if (err || (response && response.failure > 0)) {
            LogService.log('error', TAG + ACTION, err || response);
            return callback(ErrorService.raise('PUSH_FAILED'));
          } else {
            return callback(null);
          }
        });

        // Don't wait up
        offerData.save(function(err) {
          if (err) {
            LogService.log('error', TAG + ACTION, err);
          }
        });
      }],

      reject: ['findUser', 'findInquiry', 'findOthers', function (callback, cbres) {
        var inquiryData = cbres.findInquiry;
        var userData = cbres.findUser;
        var offersArr = cbres.findOthers;
        var regTokens = [];

        async.map(offersArr, function (offerData, next) {
          offerData.status = 'REJECTED';

          var message = {
            type: 'OFFER',
            offerId: offerData.id,
            status: offerData.status,
            details: inquiryData.toJSON()
          };

          // Add to data
          var dataBody = {
            type: 'OFFER',
            status: offerData.status,
            details: inquiryData.toJSON(),
            profile: offerData.createdBy
          };
          DataService.log(dataBody);

          // Send push notifs
          GCMService.send([offerData.createdBy.gcmToken], message, function (err, response) {
            if (err || (response && response.failure > 0)) {
              LogService.log('error', TAG + ACTION, err || response);
              return next(ErrorService.raise('PUSH_FAILED'));
            } else {
              return next(null);
            }
          });

          // Don't wait up
          offerData.save(function(err) {
            if (err) {
              LogService.log('error', TAG + ACTION, err);
            }
          });
        }, callback)
      }]
    }, function (error, output) {
      var resp = {};
      if (error) {
        resp = { error: error.error };
        LogService.log('error', TAG + ACTION + ' response', resp);
        res.json(error.status, resp);
      } else {
        resp = { success: 'ok' };
        LogService.log('debug', TAG + ACTION + ' response', resp);
        res.json(200, resp);
      }
    });
  },

  getAllOffers: function (req, res) {
    var ACTION = '[getAllOffers]';

    LogService.log('debug', TAG + ACTION + ' request body', req.body);

    async.auto({
      find: function (callback, cbres) {
        var query = {
          forInquiry: req.params.inquiryId
        };

        Offer.find(query)
          .populate('createdBy')
          .exec(function (err, result) {
          if (err) {
            LogService.log('error', TAG + ACTION, err);
            callback(ErrorService.raise('DB_ERROR'));
          } else {
            var obj  = {
              offers: result,
              total: result.length
            };
            callback(null, obj);
          }
        })
      }

    }, function (error, output) {
      var resp = {};

      if (error) {
        resp = { error: error.error };
        LogService.log('error', TAG + ACTION + ' response', err);
        res.json(err.status, {error: err.error});
      } else {
        LogService.log('debug', TAG + ACTION + ' response', output.find);
        res.json(200, output.find);
      }
    });
  }
}
