var TAG = '[MerchantController]';

module.exports = {

  addMerchant: function (req, res) {
    var ACTION = '[create]';
    LogService.log('debug', TAG + ACTION + ' request body', req.body);

    async.auto({
      validation: function (callback) {
        var keys = ['displayName', 'username', 'password', 'location'];
        callback(ValidationService.containKeys(keys, req.body || {}));
      },

      create: ['validation', function (callback) {
        // Flatten location
        req.body.location = [req.body.location.longitude, req.body.location.latitude];
        
        Merchant.create(req.body, function (err, result) {
          if (err) {
            LogService.log('error', TAG + ACTION, err);
            callback(ErrorService.raise('DB_ERROR'));
          } else {
            callback(null, result);
          };
        });
      }]
    }, function (error, output) {
      var resp = {};

      if (error) {
        resp = { error: error.error };
        LogService.log('error', TAG + ACTION + ' response', resp);
        res.json(error.status, resp);
      } else {
        resp = { success: 'ok', merchantId: output.create.id };
        LogService.log('debug', TAG + ACTION + ' response', resp);
        res.json(200, resp);
      }
    });
  },

  getAllMerchants: function (req, res) {
    var ACTION = '[getAllMerchants]';
    LogService.log('debug', TAG + ACTION + ' request query', req.query);

    async.auto({
      validate: function(callback) {
        var keys = ['longitude', 'latitude'];
        var qs = JSON.parse(JSON.stringify(req.query));
        var validation = ValidationService.containKeys(keys, qs);
        if (validation === null) {
          req.query.radius = parseFloat(req.query.radius);
          req.query.longitude = parseFloat(req.query.longitude);
          req.query.latitude = parseFloat(req.query.latitude);
        }
        callback(validation);
      },
      find: [ 'validate', function(callback) {
        req.query.radius = isNaN(req.query.radius) ? sails.config.mapProperties.merchants.radius : req.query.radius;
        var query = {
          location: {
            '$geoWithin': {
              '$center': [ [ req.query.longitude, req.query.latitude ], req.query.radius / sails.config.mapProperties.earthRadiusKm ]
            }
          }
        };

        Merchant.find(query, function (err, data) {
          if (err) {
            LogService.log('error', TAG + ACTION, err);
            callback(ErrorService.raise('DB_ERROR'));
          } else {
            var obj  = {
              merchants: data,
              total: data.length
            };
            callback(null, obj);
          }
        });

      }]
    }, function(err, results) {
      if (err) {
        LogService.log('error', TAG + ACTION + ' response', err);
        res.json(err.status, {error: err.error});
      } else {
        LogService.log('debug', TAG + ACTION + ' response', results.find);
        res.json(200, results.find);
      }
    });
  }
}
