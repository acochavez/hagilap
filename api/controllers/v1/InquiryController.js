var TAG = '[InquiryController]';

module.exports = {
  ask: function (req, res) {
    var ACTION = '[ask]';
    LogService.log('debug', TAG + ACTION + ' request body', req.body);

    async.auto({
      validation: function (callback) {
        var keys = ['item', 'category', 'location'];
        callback(ValidationService.containKeys(keys, req.body || {}));
      },

      create: ['validation', function (callback) {
        req.body.createdBy = req.query.userid;

        // Flatten location
        req.body.location = [req.body.location.longitude, req.body.location.latitude];
        Inquiry.create(req.body, function (err, result) {
          if (err) {
            LogService.log('error', TAG + ACTION, err);
            callback(ErrorService.raise('DB_ERROR'));
          } else {
            callback(null, result);
          };
        });
      }],

      findUser: ['validation', function (callback, cbres) {
        User.findOne({id: req.query.userid}, function (err, result) {
          if (err) {
            LogService.log('error', TAG + ACTION, err);
            callback(ErrorService.raise('DB_ERROR'));
          } else {
            callback(null, result);

            // Background
            // result.location = req.body.location;
            // result.save(function (err) {
            //   if (err) LogService.log('error', TAG + ACTION, err);
            // })
          };
        })
      }],

      pushToNearby: ['create', 'findUser', function (callback, cbres) {
        var inquiryData = cbres.create;
        var userData = cbres.findUser;

        var query = {
          location: {
            '$geoWithin': {
              '$center': [ req.body.location, sails.config.businessRules.inquiry.radius / sails.config.mapProperties.earthRadiusKm ]
            }
          }
        };

        User.find(query, function (err, data) {
          if (err) {
            LogService.log('error', TAG + ACTION, err);
            callback(ErrorService.raise('DB_ERROR'));
          } else {
            var regTokens = [];
            for (var i in data) {
              if (data[i].id != req.query.userid && data[i].gcmToken && data[i].gcmToken !== "") {  // Don't include requesting user to push
                regTokens.push(data[i].gcmToken);
              }
            }

            //if (regTokens.length === 0) return callback(ErrorService.raise('NO_NEARBY'));

            var message = {
              type: 'INQUIRY',
              details: inquiryData.toJSON(),
              profile: {
                name: userData.name,
                age: userData.age,
                gender: userData.gender,
                userType: userData.userType,
                medPrac: userData.medPrac,
                location: {
                  longitude: req.body.location[0],
                  latitude: req.body.location[1]
                }
              }
            };

            // Add to data
            var dataBody = {
              type: 'INQUIRY',
              status: 'PENDING',
              details: inquiryData.toJSON(),
              profile: userData
            };
            DataService.log(dataBody);

            // Send push notifs
            GCMService.send(regTokens, message, function (err, response) {
              if (err || (response && response.failure > 0)) {
                LogService.log('error', TAG + ACTION, err || response);
                return callback(ErrorService.raise('PUSH_FAILED'));
              } else {
                return callback(null);
              }
            });
          }
        });
      }]
    }, function (error, output) {
      var resp = {};

      if (error) {
        resp = { error: error.error };
        LogService.log('error', TAG + ACTION + ' response', resp);
        res.json(error.status, resp);
      } else {
        resp = { success: 'ok', inquiryId: output.create.id };
        LogService.log('debug', TAG + ACTION + ' response', resp);
        res.json(200, resp);
      }
    });
  },

  resolve: function (req, res) {
    var ACTION = '[resolve]';
    LogService.log('debug', TAG + ACTION + ' request body', req.body);

    async.auto({
      getInquiry: function (callback) {
        Inquiry.findOne({id: req.params.inquiryId, status: 'HOLD'})
          .populate('createdBy')
          .exec(function (err, data) {
          if (err) {
            LogService.log('error', TAG + ACTION, err);
            callback(ErrorService.raise('DB_ERROR'));
          } else if (!data) {
            LogService.log('error', TAG + ACTION, 'No such inquiry');
            callback(ErrorService.raise('NOT_FOUND'));
          } else {
            data.status = 'RESOLVED';

            callback(null, data);

            data.save(function (err) {
              if (err) {
                LogService.log('error', TAG + ACTION, err);
              }
            });
          }
        })
      },

      updatePoints: ['getInquiry', function (callback, cbres) {
        // Don't wait up
        callback(null);

        var inquiryData = cbres.getInquiry;

        // Add to data
        var dataBody = {
          type: 'INQUIRY',
          status: 'RESOLVED',
          details: inquiryData.toJSON(),
          profile: inquiryData.createdBy
        };
        DataService.log(dataBody);

        Offer.findOne({forInquiry: req.params.inquiryId, status: 'ACCEPTED'}, function (err, result) {
          if (err) {
            LogService.log('error', TAG + ACTION, err);
          } else if (!result) {
            LogService.log('error', TAG + ACTION, 'No such offer');
          } else {
            // Find user who made this offer and give him/her points
            User.findOne({id: result.createdBy}, function (err, result) {
              if (err) {
                LogService.log('error', TAG + ACTION, err);
              } else if (!result) {
                LogService.log('error', TAG + ACTION, 'No such user');
              } else {
                result.points += sails.config.businessRules.inquiry.points[inquiryData.category];
                result.save(function (err) {
                  if (err) {
                    LogService.log('error', TAG + ACTION, err);
                  }
                })
              }
            });
          }
        });
      }]
    }, function (error, output) {
      var resp = {};

      if (error) {
        resp = { error: error.error };
        LogService.log('error', TAG + ACTION + ' response', resp);
        res.json(error.status, resp);
      } else {
        resp = { success: 'ok' };
        LogService.log('debug', TAG + ACTION + ' response', resp);
        res.json(200, resp);
      }
    });
  }
}
