var TAG = '[UserController]';

module.exports = {

  register: function(req, res) {
    var ACTION = '[register]';
    LogService.log('debug', TAG + ACTION + ' request body', req.body);

    async.auto({
      validation: function(callback) {
        var keys = ['name', 'msisdn', 'age', 'gender'];
        callback(ValidationService.containKeys(keys, req.body));
      },
      getPending: ['validation', function (callback) {
        Registration.findOne({msisdn: req.body.msisdn}, function (err, data) {
          if (err) {
            LogService.log('error', TAG + ACTION, err);
            callback(ErrorService.raise('DB_ERROR'));
          } else {
            if (data) {
              if (data.resendCnt >= sails.config.businessRules.vcode.maxResend)
                callback(ErrorService.raise('MAX_RESEND_VCODE'));
              else
                callback(null, data);
            } else {
              callback(null);
            }
          }
        })
      }],
      register: ['getPending', function (callback, result) {
        var user = result.getPending;
        var vcode = '' + Math.floor(Math.random() * (10000 - 1000) + 1000);

        //If there's existing reg, let's just update the data
        if (user) {
          user.name = req.body.name;
          user.age = req.body.age;
          user.gender = req.body.gender;
          user.vcode = vcode;
          user.resendCnt++;

          user.save(function (err) {
            if (err) {
              LogService.log('error', TAG + ACTION, err);
              callback(ErrorService.raise('DB_ERROR'));
            } else {
              callback(null, user);
            }
          })
        } else {   //Create new record
          var obj = {
            name: req.body.name,
            msisdn: req.body.msisdn,
            age: req.body.age,
            gender: req.body.gender,
            vcode: vcode
          };

          Registration.create(obj, function (err, data) {
            if (err) {
              LogService.log('error', TAG + ACTION, err);
              callback(ErrorService.raise('DB_ERROR'));
            } else {
              callback(null, data)
            };
          });
        }
      }],
      sendVerificationCode: ['register', function (callback, result) {
        var vcode = result.register.vcode;
        var message = sails.config.spiels.SMS.verification.replace('%ver_code%', vcode);
        SMSService.send(req.body.msisdn, message);
        callback(null);
      }],
      setExpiry: [ 'register', function(callback, result) {
        Registration.native(function (err, connection) {
          if (err) {
            LogService.log('error', TAG + ACTION, err);
            return callback(ErrorService.raise('DB_ERROR'));
          }
          connection.expire(sails.config.connections.redis.prefix + result.register.id,
            sails.config.businessRules.vcode.ttl, function (err, res) {
              if (err) {
                LogService.log('error', TAG + ACTION, err);
                return callback(ErrorService.raise('DB_ERROR'));
              } else { callback(); }
          });
        });
      }]
    }, function (err, results) {
      if (err) {
        LogService.log('error', TAG + ACTION + ' response', {error: err.error});
        res.json(err.status, {error: err.error});
      } else {
        LogService.log('debug', TAG + ACTION + ' response', results.register );
        res.json(200, results.register);
      }
    });
  },

  verify: function(req, res) {
    var ACTION = '[verify]';
    LogService.log('debug', TAG + ACTION + ' request body', req.body);
    async.auto({
      validation: function(callback) {
        var keys = ['vcode', 'msisdn'];
        callback(ValidationService.containKeys(keys, req.body));
      },
      verifyUser: [ 'validation', function(callback) {
        Registration.findOne({msisdn: req.body.msisdn}, function(err, data) {
          if (err) {
            LogService.log('error', TAG + ACTION, err);
            callback(ErrorService.raise('DB_ERROR'));
          } else {
            if (data === undefined) {
              callback(ErrorService.raise('NO_PENDING_REGISTRATION'));
            } else { callback(null, data); }
          }
        });
      }],
      verifyCode: [ 'verifyUser', function(callback, result) {
        var user = result.verifyUser;

        if (user.verifyCnt >= sails.config.businessRules.vcode.maxVerify)
          return callback(ErrorService.raise('MAX_VERIFY_VCODE'));

        if (req.body.vcode === user.vcode) {
          Registration.destroy({msisdn: req.body.msisdn}, function(err, data) {
            if (err) {
              LogService.log('error', TAG + ACTION, err);
              callback(ErrorService.raise('DB_ERROR'));
            } else { callback(); }
          });
        } else {
          //Update verify count
          user.verifyCnt++;

          user.save(function (err) {
            if (err) {
              LogService.log('error', TAG + ACTION, err);
              callback(ErrorService.raise('DB_ERROR'));
            } else {
              LogService.log('error', TAG + ACTION, 'Incorrect verification code.');
              callback(ErrorService.raise('INCORRECT_VCODE'));
            }
          })
        }
      }],
      saveNewUser: [ 'verifyCode', function(callback, result) {
        var obj = {
          name: result.verifyUser.name,
          msisdn: result.verifyUser.msisdn,
          age: result.verifyUser.age,
          gender: result.verifyUser.gender
        };
        User.findOne({msisdn: req.body.msisdn}, function (err, data) {
          if (err) {
            LogService.log('error', TAG + ACTION, err);
            callback(ErrorService.raise('DB_ERROR'));
          } else {
            if (data === undefined) {
              User.create(obj, function(err, data) {
                if (err) {
                  LogService.log('error', TAG + ACTION, err);
                  callback(ErrorService.raise('DB_ERROR'));
                } else { callback(null, data); }
              });
            } else {
              //Update name
              data.name = obj.name;

              data.save(function (err) {
                if (err) {
                  LogService.log('error', TAG + ACTION, err);
                  callback(ErrorService.raise('DB_ERROR'));
                } else {
                  callback(null, data);
                }
              })
            }
          }
        });
      }],
      getToken: ['saveNewUser', function (callback, result) {
        TokenService.generateToken(result.saveNewUser.id, req.body.msisdn, function(err, result){
          if (err) {
            LogService.log('error', TAG + ACTION, err);
            callback(ErrorService.raise('INTERNAL_SERVER_ERROR'));
          } else { callback(null, result); }
        });
      }],
    }, function(err, results) {
      if (err) {
        LogService.log('error', TAG + ACTION + ' response', {error: err.error});
        res.json(err.status, {error: err.error});
      } else {
        var obj = UtilityService.formatUserDetail(results.saveNewUser.toJSON());
        obj.token = results.getToken;
        LogService.log('debug', TAG + ACTION + ' response', obj );
        res.json(200, obj);
      }
    });
  },

  getUserDetails: function(req, res) {
    var ACTION = '[show]';
    LogService.log('debug', TAG + ACTION + ' request query', req.query);
    async.auto({
      get: function(callback) {
        User.findOne({id: req.query.userid}, function(err, data) {
          if (err) {
            LogService.log('error', TAG + ACTION, err);
            callback(ErrorService.raise('DB_ERROR'));
          } else {
            if (data === undefined) {
              callback(ErrorService.raise('NOT_FOUND'));
            } else {
              callback(null, data);
            }
          }
        });
      }
    }, function(err, results) {
      if (err) {
        LogService.log('error', TAG + ACTION + ' response', err);
        res.json(err.status, {error: err.error});
      } else {
        var obj = {
          profile: results.get.toJSON()
        };
        delete obj.profile.id;
        LogService.log('debug', TAG + ACTION + ' response', obj);
        res.json(200, obj);
      }
    });
  },

  updateUserDetails: function(req, res) {
    var ACTION = '[update]';
    LogService.log('debug', TAG + ACTION + ' request body', req.body);

    async.auto({
      getUser: function (callback) {
        User.findOne({id: req.query.userid}, callback);
      },
      update: [ 'getUser', function (callback, cbres) {
        var obj = {};

        if (req.body.name)  obj.name = req.body.name;
        if (req.body.msisdn)  obj.msisdn = req.body.msisdn;
        if (req.body.age)  obj.age = req.body.age;
        if (req.body.gender)  obj.gender = req.body.gender;
        if (req.body.gcmToken)  obj.gcmToken = req.body.gcmToken;
        if (req.body.location)  obj.location = [req.body.location.longitude, req.body.location.latitude];

        User.update({id: req.query.userid}, obj, function(err, data) {
          if (err) {
            LogService.log('error', TAG + ACTION, err);
            callback(ErrorService.raise('DB_ERROR'));
          } else {
            if (data === undefined) {
              callback(ErrorService.raise('NOT_FOUND'));
            } else {
              callback(null, data[0]);
            }
          }
        });
      }]
    }, function(err, results) {
      if (err) {
        LogService.log('error', TAG + ACTION + ' response', err);
        res.json(err.status, {error: err.error});
      } else {
        var obj = {
          profile: results.update.toJSON()
        };
        delete obj.profile.id;
        LogService.log('debug', TAG + ACTION + ' response', obj);
        res.json(200, obj);
      }
    });
  }

}
