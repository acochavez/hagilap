var TAG = '[SessionController]';

module.exports = {
  index: function (req, res) {
    var ACTION = '[index]';
    LogService.log('debug', TAG + ACTION);

    res.view('homepage', {prompt: ''});
  },

  dashboard: function (req, res) {
    var ACTION = '[dashboard]';
    LogService.log('debug', TAG + ACTION);

    res.view('dashboard', {username: req.session.username});
  },

  createDeal: function (req, res) {
    var ACTION = '[createDeal]';
    LogService.log('debug', TAG + ACTION);

    res.view('createDeal');
  },

  useDeal: function (req, res) {
    var ACTION = '[useDeal]';
    LogService.log('debug', TAG + ACTION);

    res.view('useDeal');
  },

  login: function (req, res) {
    var ACTION = '[login]';
    LogService.log('debug', TAG + ACTION + ' request body', req.body);

    var query = {
          username: req.body.username
        },
        prompt = '';

    Merchant.findOne(query, function (err, result) {
      if (err) {
        prompt = PromptService.raise('UNKNOWN_ERROR');
      } else {
        if (result) {
          //Verify password
          if (CryptoService.verifyPassword(result.password, result.salt, req.body.password)) {
            req.session.merchantId = result.id;
            req.session.username = result.username;

            return res.redirect('/dashboard');
          } else {
            prompt = PromptService.raise('INVALID_CREDENTIALS');
          }
        } else {
          prompt = PromptService.raise('INVALID_CREDENTIALS');
        }
      }

      //Stay on login page and prompt user accordingly
      res.view('homepage', {prompt: prompt});
    })
  },

  logout: function (req, res) {
    //Delete session
    if (req.session.merchantId) { delete req.session.merchantId; }
    if (req.session.username) { delete req.session.username; }

    res.redirect('/');
  }
}
