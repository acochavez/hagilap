var TAG = '[isAuthenticated]';

module.exports = function (req, res, next) {
  LogService.log('debug', TAG, req.path);

  if (req.headers.token) {
    LogService.log('verbose', TAG, req.headers.token);

    TokenService.verifyToken(req.headers.token, function (err, result){
      if (err) {
        LogService.log('error', TAG, err);
        return res.json(err.status, {error : err.error});
      } else {
        req.query.userid = result.id;
        req.query.msisdn = result.msisdn;
        return next();
      }
    });
  } else {
    var err = ErrorService.raise('UNAUTHORIZED');

    LogService.log('error', TAG, err);

    return res.json(err.status, {error: err.error});
  }
};
