var TAG = '[isLoggedIn]';

module.exports = function (req, res, next) {
  sails.log.debug(TAG);

  if (req.session.merchantId) {
    req.params.merchantId = req.session.merchantId;
    next();
  } else {
    res.redirect('/');
  }
}
