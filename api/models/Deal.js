var TAG = '[Deal]';

module.exports = {
  schema: true,
  connection: 'mongo',

  attributes: {
    name: {
      type: 'string',
      required: true
    },

    description: {
      type: 'string',
      required: true
    },

    createdBy: {
      model: 'merchant',
      required: true
    },

    toJSON: function() {
      var obj = this.toObject();
      //Remove system attributes
      delete obj.createdAt;
      delete obj.updatedAt;
      delete obj.createdBy;
      return obj;
    }
  }
}
