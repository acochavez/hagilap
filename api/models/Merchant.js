var TAG = '[Merchant]',
    urlLib = require('url'),
    validator = require('validator');

module.exports = {
  schema: true,
  connection: 'mongo',

  types: {
    point: function (coor) {
      return parseFloat(coor[0]) && parseFloat(coor[1]);
    },

    isURL: function (url) {
      return validator.isURL(url, {require_protocol: true});
    }
  },

  attributes: {

    displayName: {
      type: 'string',
      required: true
    },

    logo: {
      type: 'string',
      isURL: true,
      defaultsTo: ''
    },

    username: {
      type: 'string',
      required: true,
      unique: true
    },

    password: {
      type: 'string',
      required: true
    },

    salt: {
      type: 'string',
      required: true
    },

    location: {
      type: 'array',
      required: true,
      point: true
      /*
        [longitude, latitude]
       */
    },

    redeemedPoints: {
      type: 'integer',
      defaultsTo: 0
    },

    toJSON: function() {
      var obj = this.toObject();
      //Remove system attributes
      delete obj.createdAt;
      delete obj.updatedAt;
      delete obj.salt;
      delete obj.password;
      delete obj.username;
      delete obj.redeemedPoints;
      return obj;
    }

  },

  beforeValidate: function (values, callback) {
    var ACTION = '[beforeValidate]';
    LogService.log('debug', TAG + ACTION, values);

    CryptoService.encryptPassword(values.password, function (err, hash, salt) {
      if (err) {
        sails.log.error(TAG, ACTION, err);
        return callback(err);
      };

      values.password = hash;
      values.salt = salt;

      callback();
    });
  },
}
