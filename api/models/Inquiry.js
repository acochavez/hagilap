var TAG = '[Inquiry]';

module.exports = {
  schema: true,
  connection: 'mongo',

  types: {
    point: function (coor) {
      return parseFloat(coor[0]) && parseFloat(coor[1]);
    },
  },

  attributes: {
    item: {
      type: 'string',
      required: true
    },
    category: {
      type: 'string',
      required: true,
      enum: ['MEDICINE', 'EQUIPMENT', 'EMERGENCY']
    },
    status: {
      type: 'string',
      defaultsTo: 'PENDING',
      enum: ['PENDING', 'HOLD', 'RESOLVED']
    },
    location: {
      type: 'array',
      point: true
      /*
        longitude: float
        latitude: float
       */
    },
    createdBy: {
      model: 'user',
      required: true
    },

    toJSON: function() {
      var obj = this.toObject();
      //Remove system attributes
      delete obj.createdAt;
      delete obj.updatedAt;
      delete obj.createdBy;
      delete obj.location;
      return obj;
    }
  }
}
