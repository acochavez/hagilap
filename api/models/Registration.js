module.exports = {
  schema: true,
  connection: 'redis',

  attributes: {
    name: {
      type: 'string',
      required: true
    },
    msisdn: {
      type: 'string',
      required: true
    },
    age: {
      type: 'integer',
      required: true
    },
    gender: {
      type: 'string',
      required: true,
      enum: ['FEMALE', 'MALE']
    },
    vcode: {
      type: 'string',
      required: true
    },
    resendCnt: {
      type: 'integer',
      defaultsTo: 0
    },
    verifyCnt: {
      type: 'integer',
      defaultsTo: 0
    },
    toJSON: function() {
      var obj = this.toObject();
      delete obj.id;
      delete obj.createdAt;
      delete obj.updatedAt;
      return obj;
    }
  }
};
