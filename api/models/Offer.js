var TAG = '[Offer]';

module.exports = {
  schema: true,
  connection: 'mongo',

  types: {
    point: function (coor) {
      return parseFloat(coor[0]) && parseFloat(coor[1]);
    },
  },

  attributes: {
    location: {
      type: 'array',
      point: true
      /*
        longitude: float
        latitude: float
       */
    },
    status: {
      type: 'string',
      defaultsTo: 'AWAITING',
      enum: ['AWAITING', 'ACCEPTED', 'REJECTED']
    },
    createdBy: {
      model: 'user',
      required: true
    },
    forInquiry: {
      model: 'inquiry',
      required: true
    },

    toJSON: function() {
      var obj = this.toObject();
      //Remove system attributes
      delete obj.createdAt;
      delete obj.updatedAt;
      return obj;
    }
  }
}
