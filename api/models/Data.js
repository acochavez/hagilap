module.exports = {
  schema: true,
  connection: 'mysql',

  attributes: {

    ts: {
      type: 'datetime',
      defaultsTo: function () { return new Date(); }
    },

    type: {
      type: 'string',
      required: true
    },

    status: {
      type: 'string',
      required: true
    },

    userId: {
      type: 'string',
      required: true
    },

    userAge: {
      type: 'integer',
      required: true
    },

    userGender: {
      type: 'string',
      required: true,
      enum: ['FEMALE', 'MALE']
    },

    userLong: {
      type: 'float',
      required: true,
    },

    userLat: {
      type: 'float',
      required: true,
    },

    item: {
      type: 'string',
      required: true
    },

    category: {
      type: 'string',
      required: true,
      enum: ['MEDICINE', 'EQUIPMENT', 'EMERGENCY']
    }
  }
};
