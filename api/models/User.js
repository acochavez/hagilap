module.exports = {
  schema: true,
  connection: 'mongo',

  types: {
    point: function (coor) {
      return parseFloat(coor[0]) && parseFloat(coor[1]);
    },
  },

  attributes: {
    name: {
      type: 'string',
      required: true
    },
    msisdn: {
      type: 'string',
      unique: true,
      required: true
    },
    age: {
      type: 'integer',
      required: true
    },
    gender: {
      type: 'string',
      required: true,
      enum: ['FEMALE', 'MALE']
    },
    points: {
      type: 'integer',
      defaultsTo: 0
    },
    userType: {
      type: 'string',
      defaultsTo: 'USER',
      enum: ['USER', 'HOSPITAL']
    },
    medPrac: {
      type: 'boolean',
      defaultsTo: false
    },
    gcmToken: {
      type: 'string',
      defaultsTo: ''
    },
    location: {
      type: 'array',
      point: true
      /*
        longitude: float
        latitude: float
       */
    },
    toJSON: function() {
      var obj = this.toObject();
      //Remove system attributes
      delete obj.createdAt;
      delete obj.updatedAt;
      delete obj.gcmToken;
      //delete obj.location;
      return obj;
    }
  }
};
