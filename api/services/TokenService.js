var TAG = '[TokenService]';
var crypto = require('crypto');

module.exports = {

  generateToken: function (id, msisdn, callback) {
    var cipher = crypto.createCipher(sails.config.session.algo, sails.config.session.secret);
    var now = Math.round(+new Date);
    var info = 'id:' + id + ',' +
            'msisdn:' + msisdn + ',' +
             'created_at:' + now + ',' +
             'expires_at:' + (now + sails.config.session.ttl);
    var crypted = cipher.update('' + info, 'utf8', 'hex');

    callback(null, crypted += cipher.final('hex'));
  },

  verifyToken: function (token, callback) {
    var decipher = crypto.createDecipher(sails.config.session.algo, sails.config.session.secret);
    var now = Math.round(+new Date);
    var info = decipher.update(token, 'hex', 'utf8');
    var obj = {};
    var temp = '';
    info += decipher.final('utf8');

    if (!info) {
      return callback(ErrorService.raise('EXPIRED_TOKEN'));
    } else {
      //Check expiry
      info = info.split(',');
      info.forEach(function(prop) {
        temp = prop.split(':');
        obj[temp[0]] = temp[1];
      });
      if (typeof(obj.id) === 'undefined'|| typeof(obj.msisdn) === 'undefined' || typeof(obj.created_at) === 'undefined' || typeof(obj.expires_at) === 'undefined') {
        return callback(ErrorService.raise('MALFORMED_TOKEN'));
      } else if (parseInt(obj.expires_at) < now) {
        return callback(ErrorService.raise('EXPIRED_TOKEN'));
      } else {
        return callback(null, obj);
      }
    }    
  },
}
