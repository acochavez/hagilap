var TAG = '[SMSService]';

module.exports = {

  send: function(msisdn, message) {
    var ACTION = '[send]';
    var smsType = MSISDNService.PH_MIN.test(msisdn) ? 'local' : 'international';

    if (sails.config.sms.mock) { //Mock sms sending
      LogService.log('info', TAG + ACTION + '[mock]', {msisdn: msisdn, message: message});
    } else {
      LogService.log('info', TAG + ACTION + '[' + smsType + ']', {service: sails.config.sms[smsType], msisdn: msisdn, message: message});
      var arrayMessage = message.trim().match(/.{1,118}/g);
      arrayMessage.forEach(function(msg, index){
        var counter = arrayMessage.length > 1 ? "[" + (index + 1) + "/" + arrayMessage.length + "]" : "";
        SMSUtility[sails.config.sms[smsType]].sendSMS(msisdn, counter + msg);
      });
    }
  },

}
