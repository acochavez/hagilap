module.exports = {
  errors: function() {
    return {
      INTERNAL_SERVER_ERROR: { status: 500, error: { code: -1, msg: 'Internal server error.' } },
      DB_ERROR: { status: 503, error: { code: -2, msg: 'Database error/unavailable.' } },
      SERVICE_ERROR: { status: 503, error: { code: -3, msg: 'Service error/unavailable.' } }, //used by SMS
      NOT_FOUND: { status: 404, error: { code: -4, msg: 'Details not found.' } },
      UNAUTHORIZED: { status: 401, error: { code: -5, msg: 'Unauthorized.' } },
      MALFORMED_TOKEN: { status: 401, error: { code: -6, msg: 'Not authorized. Malformed token.' } },
      EXPIRED_TOKEN: { status: 401, error: { code: -7, msg: 'Not authorized. Token already expired.' } },
      MISSING_INVALID_PARAMS: { status: 400, error: { code: -8, msg: 'Missing/invalid parameters.', 'params': [] } },
      NO_PENDING_REGISTRATION: { status: 404, error: { code: -9, msg: 'No pending registration.' } },
      MAX_RESEND_VCODE: { status: 400, error: { code: -10, msg: 'Max resend of verification code reached. Please try again after 24 hours.' } },
      MAX_VERIFY_VCODE: { status: 400, error: { code: -11, msg: 'Max verification tries reached. Please try again after 24 hours.' } },
      INCORRECT_VCODE: { status: 400, error: { code: -12, msg: 'Incorrect verification code.' } },
      PUSH_FAILED: { status: 503, error: { code: -13, msg: 'Unable to send inquiry. Please try again.' } },
      NO_NEARBY: { status: 404, error: { code: -14, msg: 'No nearby help available.' } },
    };
  },
  raise: function(e) {
    return JSON.parse(JSON.stringify(this.errors()[e]));
  },
  param: {
    name: { field: 'name',  desc: 'Required. Must be a string.' },
    msisdn: { field: 'msisdn', desc: 'Required. Must be a valid mobile number.'},
    age: { field: 'age', desc: 'Required. Must be a valid integer.'},
    gender: { field: 'gender', desc: 'Required. Must be either FEMALE/MALE.'},
    vcode: { field: 'vcode', desc: 'Required. Must be a valid verification code.'},
    displayName: { field: 'displayName',  desc: 'Required. Must be a string.' },
    username: { field: 'username',  desc: 'Required. Must be a string.' },
    password: { field: 'password',  desc: 'Required. Must be a string.' },
    location: { field: 'location', desc: 'Required. Must be longitude - latitude pair.'},
    longitude: { field: "longitude",  desc: "Location's longitude." },
    latitude: { field: "latitude",  desc: "Location's latitude." },
    radius: { field: "radius",  desc: "Radius of location." },
    item: { field: 'item',  desc: 'Required. Must be a string.' },
    category: { field: 'category',  desc: 'Required. Must be one of the following: MEDICINE, EQUIPMENT or EMERGENCY.' },
  }
}
