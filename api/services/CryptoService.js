var TAG = '[CryptoService]',
    crypto = require('crypto');

module.exports = {
  encryptPassword: function (rawPassword, callback) {
    var salt = crypto.randomBytes(256).toString('base64',0,30),
        hashPassword = crypto.createHash('sha256').update(rawPassword + salt).digest('hex');

    callback(null, hashPassword, salt);
  },

  verifyPassword: function (hashPassword, salt, rawPassword) {
    var newHashPass = crypto.createHash('sha256').update(rawPassword + salt).digest('hex');

    return newHashPass === hashPassword;
  },

  generateToken: function (info, ttl, callback) {
    var cipher = crypto.createCipher(sails.config.tokens.algo, sails.config.tokens.secret),
        now = Math.round(+new Date),
        crypted;

    info.createdAt = now;
    info.expiresAt = now + (ttl || sails.config.tokens.ttl);

    crypted = cipher.update(JSON.stringify(info), 'utf8', 'hex');

    callback(null, crypted += cipher.final('hex'));
  },

  verifyToken: function (token, callback) {
    var decipher = crypto.createDecipher(sails.config.tokens.algo, sails.config.tokens.secret),
        now = Math.round(+new Date),
        info = decipher.update(token, 'hex', 'utf8');

    info += decipher.final('utf8');

    if (!info) {
      return callback(ErrorService.raise('UNAUTHORIZED'));
    } else {
      info = JSON.parse(info);

      //Check expiry
      if (parseInt(info.expiresAt) < now) {
        callback(ErrorService.raise('UNAUTHORIZED'));
      } else {
        callback(null, info);
      }
    }
  }
}
