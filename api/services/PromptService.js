module.exports = {
  spiels: function() {
    return {
      UNKNOWN_ERROR: 'An unknown error occured. Please try again.',
      INVALID_CREDENTIALS: 'Invalid username and/or password. Please try again.'
    };
  },
  raise: function(e) {
    return JSON.parse(JSON.stringify(this.spiels()[e]));
  }
}
