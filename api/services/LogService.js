module.exports = {

  log: function(type, tag, body) {
    body = formatObject(body);
    if (type == 'error') {
      sails.log.error(new Date().toISOString(), tag + " error :\n", body);
    } else {
      sails.log[type](new Date().toISOString(), tag + " :\n", body);
    }
  },

}

function formatObject(obj) {
  if ((typeof obj) == 'object' && !Array.isArray(obj)) {
      obj = JSON.parse(JSON.stringify(obj));
      if (obj.password) {
        obj.password = '[HIDDEN]';
      }
      obj = JSON.stringify(obj);
    }
    if (Array.isArray(obj) && obj.length > 0) {
      obj = JSON.stringify(obj);
    }
    return obj;
}
