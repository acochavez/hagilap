var TAG = '[MSISDNService]';
var PH_MIN = new RegExp(/^([\+]{0,1}63[89]|[89]|0[89]){1}\d{9}$/);

module.exports = {

  PH_MIN: PH_MIN,

  convertToPH: function(msisdn) {
    if (PH_MIN.test(msisdn)) {
      msisdn = msisdn.replace(/^\+/,'');
      msisdn = msisdn.replace(/^0/,'63');
      msisdn = msisdn.replace(/^9(\d{9})/,'639$1');
      //min = min.replace(/^8(\d{9})/,'638$1');
    }
    return msisdn;
  }

}
