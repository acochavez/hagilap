module.exports = {

  log: function (data) {
    var body = {
      ts: new Date(),
      type: data.type,
      status: data.status,
      userId: data.profile.id,
      userAge: data.profile.age,
      userGender: data.profile.gender,
      userLong: data.profile.location[0],
      userLat: data.profile.location[1],
      item: data.details.item.toLowerCase(),
      category: data.details.category
    };

  Data.create(body, function (err) {
    if (err)
      LogService.log('error', '[DataService]', err);
  });
  }
}
