module.exports = {

  containKeys: function(params, obj) {
    var err_obj = ErrorService.raise('MISSING_INVALID_PARAMS');
    params.forEach(function(param){
      if (!obj.hasOwnProperty(param) || typeof obj[param] === 'undefined'){
        err_obj.error.params.push(ErrorService.param[param]);
      } else {
        if (param == 'profile' && !isValidProfile(obj.profile)) {
            err_obj.error.params.push(ErrorService.param['profile']);
        }
      }
    });
    if (err_obj.error.params.length == 0) {
      return null;
    } else {
      return err_obj;
    }
  },

  validateKeys: function(params, obj) {
    var err_obj = ErrorService.raise('MISSING_INVALID_PARAMS');
    params.forEach(function(param) {
      if (param == 'contacts' && obj.contacts) {
        if (!isValidContacts(obj.contacts)) {
          err_obj.error.params.push(ErrorService.param['contacts']);
        }
      }
    });
    if (err_obj.error.params.length == 0) {
      return null;
    } else {
      return err_obj;
    }
  }

}

function isValidProfile(profile) {
  var valid = true;
  if (!profile.hasOwnProperty('name') || !profile.hasOwnProperty('msisdn')) {
    valid = false;
  }
  return valid;
}

function isValidContacts(contacts) {
  var valid = true;
  for (var i in contacts) {
    if (!contacts[i].hasOwnProperty('name') || !contacts[i].hasOwnProperty('msisdn')) {
      valid = false;
      break;
    }
  }
  return valid;
}
