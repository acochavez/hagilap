
module.exports = {

  formatUserDetail: function(profile) {
    var obj = {
      profile: {
        name: profile.name,
        msisdn: profile.msisdn,
        age: profile.age,
        gender: profile.gender,
        medPrac: profile.medPrac,
        userType: profile.userType
      }
    };
    return obj;
  },

  getObject: function (array, key, value) {
    for (var i = 0; i < array.length; i++) {
      if (array[i][key] === value) {
        var obj = {
          obj: array[i],
          index: i
        };
        return obj;
      }
    }
    return ({
      obj: {},
      index: -1
    });
  },

  getObjectArr: function (array, keys, values) {
    for (var i = 0; i < array.length; i++) {
      var allMatch = true;
      for (var j = 0; j < keys.length; j++) {
        //Find both keys in the element
        if (array[i][keys[j]] !== values[j]) {
          allMatch = false;
          break;
        }
      }

      if (allMatch) {
        var obj = {
          obj: array[i],
          index: i
        };
        return obj;
      }
    }
    return ({
      obj: {},
      index: -1
    });
  },

}
