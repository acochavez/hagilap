var request = require('request');

module.exports = {
  send: function (to, message, callback) {
    var options = {
      url: sails.config.push.url,
      headers: {
        Authorization: 'key=' + sails.config.push.apiKey
      },
      json: {registration_ids: to, data: { message: message } }
    };

    switch (message.type) {
      case 'INQUIRY':
        options.json.notification = {
          title: 'Hagilap Inquiry',
		      icon: 'ic_launcher',
		      body: 'Someone needs your help!'
	      };
        break;
      case 'OFFER':
        var body;
        options.json.notification = {
          title: 'Hagilap Offer',
          icon: 'ic_launcher',
          body: 'Offer status changed to: ' + message.status
        };
        break;
      default:
    };

    request.post(options, function (error, response, body) {
      callback(error, body);
    });
  }
}
