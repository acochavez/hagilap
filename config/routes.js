/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes map URLs to views and controllers.
 *
 * If Sails receives a URL that doesn't match any of the routes below,
 * it will check for matching files (images, scripts, stylesheets, etc.)
 * in your assets directory.  e.g. `http://localhost:1337/images/foo.jpg`
 * might match an image file: `/assets/images/foo.jpg`
 *
 * Finally, if those don't match either, the default 404 handler is triggered.
 * See `api/responses/notFound.js` to adjust your app's 404 logic.
 *
 * Note: Sails doesn't ACTUALLY serve stuff from `assets`-- the default Gruntfile in Sails copies
 * flat files from `assets` to `.tmp/public`.  This allows you to do things like compile LESS or
 * CoffeeScript for the front-end.
 *
 * For more information on configuring custom routes, check out:
 * http://sailsjs.org/#!/documentation/concepts/Routes/RouteTargetSyntax.html
 */

module.exports.routes = {

  /***************************************************************************
  *                                                                          *
  * Make the view located at `views/homepage.ejs` (or `views/homepage.jade`, *
  * etc. depending on your default view engine) your home page.              *
  *                                                                          *
  * (Alternatively, remove this and add an `index.html` file in your         *
  * `assets` directory)                                                      *
  *                                                                          *
  ***************************************************************************/

  /*
   * Internal user only
   */
  'POST /v1/merchants' : {
    controller: 'v1/Merchant',
    action: 'addMerchant'
  },

  /*
   * CMS Specific
   */
  'GET /': {
    controller: 'cms/Session',
    action: 'index'
  },

  'GET /dashboard': {
    controller: 'cms/Session',
    action: 'dashboard'
  },

  'GET /deals/new': {
    controller: 'cms/Session',
    action: 'createDeal'
  },

  'GET /deals/redeem': {
    controller: 'cms/Session',
    action: 'useDeal'
  },

  'POST /login': {
    controller: 'cms/Session',
    action: 'login'
  },

  'GET /logout': {
    controller: 'cms/Session',
    action: 'logout'
  },

  'POST /v1/merchants/:merchantId/deals': {
    controller: 'v1/Deal',
    action: 'addDeal'
  },

  //
  // 'POST /v1/users/:userId/deals/:dealId' : {
  //   controller: 'v1/User',
  //   action: 'redeem'
  // },
  //

  /*
   * Client APIs
   */
  'POST /v1/users' : {
    controller: 'v1/User',
    action: 'register'
  },

  'POST /v1/users/verify' : {
    controller: 'v1/User',
    action: 'verify'
  },

  'GET /v1/users/me' : {
    controller: 'v1/User',
    action: 'getUserDetails'
  },

  'PATCH /v1/users/me' : {
    controller: 'v1/User',
    action: 'updateUserDetails'
  },

  'GET /v1/merchants' : {
    controller: 'v1/Merchant',
    action: 'getAllMerchants'
  },

  'GET /v1/merchants/:merchantId/deals' : {
    controller: 'v1/Deal',
    action: 'getAllDeals'
  },

  'POST /v1/inquiries' : {
    controller: 'v1/Inquiry',
    action: 'ask'
  },

  'DELETE /v1/inquiries/:inquiryId' : {
    controller: 'v1/Inquiry',
    action: 'resolve'
  },

  'POST /v1/inquiries/:inquiryId/offers': {
    controller: 'v1/Offer',
    action: 'newOffer'
  },

  'GET /v1/inquiries/:inquiryId/offers': {
    controller: 'v1/Offer',
    action: 'getAllOffers'
  },

  'DELETE /v1/inquiries/:inquiryId/offers/:offerId': {
    controller: 'v1/Offer',
    action: 'getOffer'
  }

  /***************************************************************************
  *                                                                          *
  * Custom routes here...                                                    *
  *                                                                          *
  * If a request to a URL doesn't match any of the custom routes above, it   *
  * is matched against Sails route blueprints. See `config/blueprints.js`    *
  * for configuration options and examples.                                  *
  *                                                                          *
  ***************************************************************************/

};
