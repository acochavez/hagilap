/**
 * Development environment settings
 *
 * This file can include shared settings for a development team,
 * such as API keys or remote database passwords.  If you're using
 * a version control solution for your Sails app, this file will
 * be committed to your repository unless you add it to your .gitignore
 * file.  If your repository will be publicly viewable, don't add
 * any private information to this file!
 *
 */

module.exports = {

  port: process.env.PORT || 8085,
  environment: process.env.NODE_ENV || 'development',

  log: {
    level: 'info'
  },

  session: {
    secret: 'thisisadevsecret',
    algo: 'aes-256-ctr',
    ttl: 86400000, //1 day
  },

  connections: {
    mongo: {
      adapter: 'sails-mongo',
      host: 'localhost',
      port: 27017,
      database: 'hagilap'
    },

    redis: {
      adapter: 'sails-redis',
      host: 'localhost',
      port: 6379,
      ttl: 60 * 60, //sec
      database: 1,
      prefix: 'hagilap:waterline:registration:id:'
    },

    mysql: {
      adapter: 'sails-mysql',
      host: 'localhost',
      port: 3306,
      user: 'root',
      password: '',
      database: 'hagilap'
    }
  },

  businessRules: {
    vcode: {
      ttl: 3600, //sec
      maxResend: 10,
      maxVerify: 4
    },

    inquiry: {
      radius: 5, // in KM
      points: {
        MEDICINE: 5,
        EQUIPMENT: 10,
        EMERGENCY: 0
      }
    }
  },

  push: {
    url: 'https://gcm-http.googleapis.com/gcm/send',
    apiKey: 'AIzaSyDIJ-nSsSLjPsc8gvVKhhchZEahC8holHc'
  },

  mapProperties: {
    merchants: {
      radius: 5 // in KM
    },
    earthRadiusKm: 6378.1
  },

  sms: {
    mock: true,
    local: 'chikka',
    international: 'twilio',
    chikka: {
      sendUrl: 'https://post.chikka.com/smsapi/request',
      credentials: {
        shortcode: '292902766',
        clientId: '734831126b93f10efe75ea0c09c7abc59bc4b4a5ae7fa1992955e5b99a16fd75',
        secretKey: '1cf75bc3a8ee899c3bb81bb45deeeb2a34b1e2f9a87c140b36f2289960b29d67'
      }
    },
    twilio: {
      shortcode: '+14243532766',
      sid: 'AC1bde2dd1885c0bff2b023b47970fbce0',
      token: '6d637a368949f746b0f779bbd95e9482'
    }
  }

};
