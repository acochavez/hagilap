$(document).ready(function () {
  $('#createDeal').on('submit', function (e) {
    e.preventDefault();

    var payload = {
          name: $('#name').val(),
          description: $('#description').val()
        };

    $.ajax({
      type: 'POST',
      url: '/v1/merchants/merchantId/deals',
      headers: {
        'Content-Type':'application/json'
      },
      data: JSON.stringify(payload),
      error: function (msg) {
        console.log(msg.responseText);
        $('#prompt').html('<div data-alert class="alert-box alert" style="color:red;">Error occured. Please try again.</div>');
      },
      success: function (msg) {
        console.log(msg);
        if (msg.message) {
          //Error occured
          $('#prompt').html('<div data-alert class="alert-box alert" style="color:red;">Error occured. Please try again.</div>');
        } else {
          $('#prompt').html('<div data-alert class="alert-box success" style="color:green;">Successfully added deal!</div>');
        }
      }
    });
  })

  $('#useDeal').on('submit', function (e) {
    e.preventDefault();

    var code = $('#code').val();

    $.ajax({
      type: 'DELETE',
      url: '/v1/deals/me/vouchers/' + code,
      headers: {
        'Content-Type':'application/json'
      },
      error: function (msg) {
        console.log(msg.responseText);
        $('#prompt').html('<div data-alert class="alert-box alert" style="color:red;">Error occured. Please try again.</div>');
      },
      success: function (msg) {
        console.log(msg);
        if (msg.message) {
          //Error occured
          $('#prompt').html('<div data-alert class="alert-box alert" style="color:red;">Error occured. Please try again.</div>');
        } else {
          $('#prompt').html('<div data-alert class="alert-box success" style="color:green;">Successfully redeemed deal!</div>');
        }
      }
    });
  })
})
